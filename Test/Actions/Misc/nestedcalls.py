'''
    An action demonstrating that an action can call other actions
'''

from eoq2 import Cal,Obs,His,Cmp,Ubs
from eoq2.event import EvtTypes
from eoq2.action.util import AscAndWait

def nestedcalls(domain : 'Domain'):
    print("Calling progress a first time for 5 seconds.")
    (status,res) = AscAndWait(domain,'Misc/progress',[0.5,10],forwardOutput=True)
    print("Calling progress a first time finished: status=%s, res=%s"%(status,res))
      
    print("Calling progress a second time for 2 seconds.")
    (status,res) = AscAndWait(domain,'Misc/progress',[0.2,10],forwardOutput=True)
    print("Calling progress a second time finished: status=%s, res=%s"%(status,res))
    
            
        
    
    