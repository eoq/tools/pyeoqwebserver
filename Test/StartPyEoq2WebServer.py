import os
import platform

platformIdentString = platform.platform(aliased=True, terse=True)
if "Darwin" in platformIdentString:
    from pathlib import Path
    cwd = os.getcwd()
    os.system(r'python ' + str(Path(os.getcwd()).parent) + '/PyEoq2WebServer.py --port 8000 --workspaceDir "./Workspace" --metaDir "Meta" --actions 1 --actionsDir "./Actions" --backup 1 --backupDir "./backup" --logDir "./log" --logToConsole 1 --autosave 5.0')
    print("INFO: OS X detected")
else:
    os.system(r'python ..\PyEoq2WebServer.py --port 8000 --workspaceDir "./Workspace" --metaDir "Meta" --actions 1 --actionsDir "./Actions" --backup 1 --backupDir "./backup" --logDir "./log" --logToConsole 1 --autosave 5.0 --maxChanges 5000')
